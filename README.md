# Système Dé-Bilus
![Foundry v11](https://img.shields.io/badge/foundry-v11-green)

Système Dé-Bilus pour Foundry VTT.
Ce système est basé sur le D20 System, toutes les valeurs de jet dont des modificateurs ajoutés à un jet de D20.

## Règles
Vous trouverez les règles du système sur le [Wiki de la Taverne des Rôlistes](https://wiki.taverne-des-rolistes.fr/books/version-d20-originale)

## Dépendances
Ce système nécessite l'installation des modules suivants :
- [socketlib](https://foundryvtt.com/packages/socketlib)
- [libWrapper](https://foundryvtt.com/packages/lib-wrapper)

## Modules recommandés
- [Dice So Nice!](https://foundryvtt.com/packages/dice-so-nice)
- [Tokenizer](https://foundryvtt.com/packages/vtta-tokenizer)
- [Bar Brawl](https://foundryvtt.com/packages/barbrawl)
- [Carousel Combat Tracker](https://foundryvtt.com/packages/combat-tracker-dock)
- [Monk's TokenBar](https://foundryvtt.com/packages/monks-tokenbar)
- [Monk's Combat Marker](https://foundryvtt.com/packages/monks-combat-marker)
- [Alternative Rotation](https://foundryvtt.com/packages/alternative-rotation)

## Crédits
Système conçu et développé par ***Père Kratos*** pour [La Taverne des Rôlistes](https://taverne-des-rolistes.fr).

L'usage de ce système est libre et gratuit à condition de respecter les termes de la licence [Creative Commons BY-NC-ND 4.0 DEED](https://creativecommons.org/licenses/by-nc-nd/4.0/).