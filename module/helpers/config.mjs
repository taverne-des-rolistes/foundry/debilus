export const DEBILUS = {};

/**
 * The set of Ability Scores used within the system.
 * @type {Object}
 */
DEBILUS.abilities = {
  force: 'DEBILUS.Ability.Force.long',
  agilite: 'DEBILUS.Ability.Agilite.long',
  corps: 'DEBILUS.Ability.Corps.long',
  esprit: 'DEBILUS.Ability.Esprit.long',
  charisme: 'DEBILUS.Ability.Charisme.long',
};

DEBILUS.abilityAbbreviations = {
  force: 'DEBILUS.Ability.Force.abbr',
  agilite: 'DEBILUS.Ability.Agilite.abbr',
  corps: 'DEBILUS.Ability.Corps.abbr',
  esprit: 'DEBILUS.Ability.Esprit.abbr',
  charisme: 'DEBILUS.Ability.Charisme.abbr',
};

DEBILUS.aptitudes = {
  defense: 'DEBILUS.Aptitude.Defense',
  esquive: 'DEBILUS.Aptitude.Esquive',
  melee: 'DEBILUS.Aptitude.Melee',
  tir: 'DEBILUS.Aptitude.Tir',
  furtivite: 'DEBILUS.Aptitude.Furtivite',
  perception: 'DEBILUS.Aptitude.Perception',
  savoir: 'DEBILUS.Aptitude.Savoir',
  social: 'DEBILUS.Aptitude.Social',
  magie: 'DEBILUS.Aptitude.Magie',
}
